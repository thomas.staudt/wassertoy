
open Main

(* Hardcoded information about sources, sinks, and their distances *)

let sources = [
  node "Bremen"  20;
  node "Köln"    30;
  node "Dresden" 50
]

let sinks = [
  node "Göttingen" (-30);
  node "München"   (-45);
  node "Berlin"    (-25);
]

let dmat = [
  [ 240; 730; 400 ]; (* Bremen *)
  [ 300; 570; 580 ]; (* Köln *)
  [ 330; 400; 190 ]  (* Dresden *)
]

let dmap =
  let row n s vals = List.map2 (fun s v -> ((n,s),v)) s vals in
  List.map2 (fun n vals -> row n sinks vals) sources dmat |> List.flatten


