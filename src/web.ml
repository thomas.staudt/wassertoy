
open Data
open Main

open Js_of_ocaml
open Js_of_ocaml_tyxml

module Html = Tyxml_js.Html

let root = Dom_html.getElementById_exn "root"
(* let _ = root##.innerHTML := Js.string "TEST" *)

let id i j = i ^ "_" ^ j
let parse_id str = let open String in
  let idx = index str '_' in
  let i = sub str 0 idx in
  let j = sub str (idx+1) (length str - idx - 1) in
  (i, j)

let div ?id ?c:(c=[]) children = match id with
  | Some id -> Html.(div ~a:[a_class c; a_id id] children)
  | None    -> Html.(div ~a:[a_class c] children)

let indices s = List.init (List.length s + 1) (fun i -> i + 1)

let grid sources sinks = 
  (* One div-row for each source in sources *)
  let field id = Html.(input ~a:[a_id id; a_input_type `Number] ()) in
  let entry n j = div ~c:["col-sm-1"] [ field (id n j) ] in
  let title c str = div ~c:["col-sm-1"; c] [ Html.txt str ] in
  let info ?c:(c="") n = div ~id:(name n) ~c:["col-sm-1"; c] [] in
  let row = function
    | "_t" -> List.("" :: map name sinks |> map (title "coltitle")) |> div ~c:["row"]
    | "_c" -> List.(title "" "" :: map (info ~c:"sinkvals") sinks) |> div ~c:["row"]
    | str -> let elt = title "rowtitle" str in
      let l = (elt :: List.map (entry str) (List.map name sinks)) in
      List.append l [div ~id:str ~c:["col-sm-1"; "sourcevals"] []] |> div ~c:["row"]
  in
  let container = 
    let l = List.append ("_t" :: List.map name sources) ["_c"] in
    List.map row l |> div ~c:["container"]
  in
  container |> Tyxml_js.To_dom.of_div

let () = Dom.appendChild root (grid sources sinks)

let input_element id =
  match Dom_html.(getElementById_coerce id CoerceTo.input) with
  | None -> failwith "coercion of id to input failed"
  | Some elt -> elt

let getValue id = 
  match Js.to_string (input_element id)##.value with
  | ""  -> 0
  | str -> int_of_string str


(* Construct the transport-map from given inputs *)
let transportmap sources sinks =
  let value source sink =
    id (name source) (name sink) |> getValue
  in
  let row source =
    List.map (fun sink -> ((source, sink), value source sink)) sinks
  in
  List.map row sources |> List.flatten

let tmap = transportmap sources sinks

let () = print_endline (string_of_int (cost tmap dmap))

let update_balances tmap =
  let fill n = 
    let maybeflip i = match n with
    | Source _ -> i
    | Sink _ -> -i
    in
    let elt = Dom_html.getElementById_exn (name n) in
    let b = balance n tmap |> maybeflip in
    elt##.innerHTML := b |> string_of_int |> Js.string;
    elt##.style##.color := match b with
    | 0 -> Js.string "green"
    | _ when b < 0 -> Js.string "red"
    | _ when b > 0 -> Js.string "black"
    | _ -> failwith "never happens"
  in
  List.iter fill (sources @ sinks)

let update_cost tmap dmap =
  let elt = (Dom_html.getElementById_exn "result") in
  elt##.innerHTML := cost tmap dmap |> string_of_int |> Js.string

(*let cost_callback () =
  let tmap = transportmap sources sinks in
  let c = cost tmap dmap in
*)

let register_input_callbacks tmap =
  let keys = List.map fst tmap in
  let ids  = List.map (fun (a,b) -> id (name a) (name b)) keys in
  let f id =
    let elt = input_element id in
    let handler _ = 
      let tmap' = transportmap sources sinks in
      update_balances tmap'; update_cost tmap' dmap; Js._false
    in
    elt##.oninput := Dom_html.handler handler
  in
  List.iter f ids

(* Main part *)
let () = register_input_callbacks tmap
let () = update_balances tmap
let () = update_cost tmap dmap




(*let () = root

let unwrap = function Some a -> a | None -> failwith "unwrapped a None"

(* Interaction with the web page *)
let root = document |> Document.querySelector "#root" |> unwrap
let () = Element.setInnerText root "TEST"
(* let div = document |> Document.createElement "div" *)

(* Element.appendChild *) *)
