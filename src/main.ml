
(* Helper functions *)

let sum l = List.fold_left (+) 0 l

let find_opt f l = match List.filter f l with
  | [] -> None
  | h :: _ -> Some h


(* Types *)

type city = {
  name : string;
  mass : int
}

type node =
  | Source of city
  | Sink of city

type map = ((node * node) * int) list

(* Getters/Setters/Constructors *)

let name = function Source c | Sink c -> c.name
let mass = function Source c | Sink c -> c.mass

let node name mass = 
  match mass >= 0 with
  | true -> Source { name; mass }
  | false -> Sink { name; mass }

let values node map = 
  let filtered = match node with
  | Source _ -> List.filter (fun x -> fst (fst x) = node) map
  | Sink _   -> List.filter (fun x -> snd (fst x) = node) map
  in
  List.map snd filtered


(* Calculating the current balance for a city given a transport map *)

let balance node tmap = match node with
  | Source _ -> mass node - sum (values node tmap)
  | Sink _   -> mass node + sum (values node tmap)


(* Check a given transport map for consistency *)

let check tmap = 
  let sources, sinks = List.(split tmap |> fst |> split) in
  let check_node n = match balance n tmap = 0 with
  | true -> `Ok ()
  | false -> `Error (name n, balance n tmap)
  in
  let error_list = 
    List.map check_node (sources @ sinks) |> 
    List.filter (function `Ok _ -> false | `Error _ -> true)
  in
  if error_list = [] then `Ok () else `Error error_list 


(* Calculate the cost for given tranpsort and distance maps *)

let cost tmap dmap =
  let keys   = List.map fst tmap in
  let masses = List.map snd tmap in
  let dists  = List.map (fun x -> List.assoc x dmap) keys in (* exn-danger *)
  List.map2 ( * ) masses dists |> sum


(* Look up nodes for given names *)

let lookup str nodes = List.(map name nodes |> find_opt ((=) str))


